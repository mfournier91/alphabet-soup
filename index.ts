/* 
Within the grid of characters, the words may appear vertical, horizontal or diagonal.
Within the grid of characters, the words may appear forwards or backwards.
Words that have spaces in them will not include spaces when hidden in the grid of characters
/*

/* MY OWN ASSUMPTIONS
Each word has exactly one solution
No need to read file as input with node / fs. Recruiter specifically said role is front-end and to submit a js solution
*/

/* Input format
3x3
A B C
D E F
G H I
ABC
AEI
*/

/* Output format
ABC 0:0 0:2
AEI 0:0 2:2
*/

//type UpperChar = 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z';
type GridLocation = [number, number]

interface LetterMap {
    [key: string] : GridLocation[]
}

type Direction = (0 | 1 | -1)[]

const directions : Direction[] = [[-1, -1], [-1,0], [-1,1],
                                [0, -1],         [0,1],
                                [1, -1], [1,0], [1,1]]

function execute(input) {
    
    const {grid, words} : {grid: string[][], words: string[]} = readInput(input);
    
    const letterMap: LetterMap = readGrid(grid);
    const results: [string, GridLocation, GridLocation][] = words.map((word) => findWord(word, letterMap))

    return formatResults(results)
}

function readInput(input: string) : {grid: string[][], words: string[] } {
    const inputLines: string[] = input.split("\n");
    const rows : number = inputLines[0].split("x").map((e) => Number(e))[0];
    const grid = inputLines.slice(1, 1 + rows).map((gridRow) => gridRow.split(" "));
    const words = inputLines.slice(rows + 1).map((word) => word.replace(" ", ""))

    return {grid, words}
}


function readGrid(grid: string[][]) : LetterMap {
    const letterMap : LetterMap = {}
    return grid.reduce((acc, row, rowIndex) => {
        row.forEach((el, colIndex) => {
            acc[el] = acc[el] ? acc[el].concat([[rowIndex, colIndex]]) : [[rowIndex, colIndex]]
        })
        return acc
    }, letterMap)
}

function findWord(word: string, letterMap: LetterMap) : [string, GridLocation, GridLocation] {
    const initialLocations: GridLocation[] = letterMap[word[0]]
    let firstLocation: GridLocation;
    let correctDirection: Direction;
    // loop over location of first letter
    for (const location of initialLocations) {
        let remainingDirections = directions;
        // loop over remaining letters
        for (let i = 1; i < word.length; i++) {
            const char = word[i];
            
            // filter out directions based on character locations
            const charLocations = letterMap[char]
            remainingDirections = remainingDirections.filter((direction) => {
                return charLocations.some((coord) => (coord[0] === location[0] + direction[0] * (i)) && (coord[1] === location[1] + direction[1] * (i)))
            })
            if (remainingDirections.length !== 1) {
                continue;
            }
            else {
                firstLocation = location;
                correctDirection = remainingDirections[0];
                // return found word
                return [word, firstLocation, calculateLastLocation(word.length, firstLocation, correctDirection)]
            }
        }
    }
    
    // if this return is executed then word not found
    return [word, [-1,-1], [-1,-1]]
}

function calculateLastLocation(wordLength: number, start: GridLocation, direction: Direction): GridLocation {
    const [row, col] = start;
    const [rowDir, colDir] = direction;
    const lastRow = row + (rowDir * (wordLength - 1));
    const lastCol = col + (colDir * (wordLength - 1));
    return [lastRow, lastCol];
}


function formatResults(results: [string, GridLocation, GridLocation][]) {
    return results.map(([word, start, end]) => `${word} ${start[0]}:${start[1]} ${end[0]}:${end[1]}`).join('\n');
}

execute(`5x5
H A S D F
G E Y B H
J K L Z X
C V B L N
G O O D O
HELLO
GOOD
BYE`)

// 'HELLO 0:0 4:4\nGOOD 4:0 4:3\nBYE 1:3 1:1'



